#Copyright (C) 2019 Joseph Brandenburg

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""Module for mode-agnostic bone functions""" 
import bpy
import mathutils
from mathutils import Vector

from . import f_hier
from . import f_curve
from . import f_gen
from . import f_name

############################################################################
# Bone Chains and Curves
############################################################################

def uValuesInChain(bones, obArm):
    lengths = []
    lenTotal = 0
    for b in bones:
        lengths.append(lenTotal)
        lenTotal += b.length
    if (lenTotal > 0):
        returnMe = []
        for l in lengths:
            returnMe.append(l/lenTotal)
        returnMe.append(1)
        return returnMe #not sure why but returning lengths doesn't work
    return [0]
            
def PointsFromBoneChain(bones, obArm):
    points = []
    location = True
    # Doesn't work with "weird" chains with connected 
    #  and disconnected parents at various points
    if (obArm.mode == 'EDIT'):
        if (bones[0].tail != bones[1].head):
            location = False
        for b in bones:
            points.append(b.head.copy())
        if (location == True):
            points.append(bones[-1].tail.copy())
    else:
        if (bones[0].tail_local != bones[1].head_local):
            location = False
        for b in bones:
            points.append(b.head_local.copy())
        if (location == True):
            points.append(bones[-1].tail_local.copy())
    return points

def SnapChainToPoints(bones, obArm, points, factor, radius = [], radiusFac = -1, roll = [], rollFac = -1):
    #ALWAYS USE THIS TO MOVE CHAINS
    #Modifying bone.tail and bone.head can lead to double transformations
    #
    #it is assumed that len(points) == (len(bones) + 1)
    #it is assumed that this feature is only called in edit mode
    #bones must be edit bones
    #TODO put in an assert, then!
    #TODO CLEANUP
    #TODO move to editBones
    #TODO maybe add envelope/roll features
    #TODO don't allow any two factors to be the same
    if (radiusFac == -1): radiusFac = factor
    if (rollFac == -1): rollFac = factor
    tailDisp = [Vector((0,0,0)) for b in bones]
    location = True
    bPrev = None
    if (bones[0].tail != bones[1].head):
        location = False
    for i, b in enumerate(bones):
        if (location == False):
            disp = (b.tail.copy() - b.head.copy())
            tailDisp[i] = ( disp )
        b.head = b.head.lerp(points[i], factor)
        # TODO, support other chains than location connected
        if (len(radius) == len(points)): # shouldn't have to check length every time
                                         # could use := here for speed up?
            b.head_radius = f_gen.lerpVal(b.head_radius, radius[i], radiusFac)
            if (bPrev):
                bPrev.tail_radius = f_gen.lerpVal(bPrev.tail_radius, radius[i], radiusFac)
            bPrev = b
    if (bPrev):
        bPrev.tail_radius = f_gen.lerpVal(bPrev.tail_radius, radius[-1], radiusFac)
        #
    if ((bones[1].use_connect == False) and (location == True)):
        for i in range(len(bones) - 1):
            b = bones[i]
            b.tail = b.tail.lerp(points[i + 1], factor)
    if (location == True):
        bones[-1].tail = (bones[-1].tail.lerp(points[-1], factor))
    else:
        for i, b in enumerate(bones):
            b.tail = (b.head.copy() + tailDisp[i])
    if (len(bones) == len(roll)):
        for r, b in zip(roll, bones):
            bRoll = b.roll
            b.align_roll(r)
            b.roll = f_gen.lerpVal(bRoll, b.roll, rollFac)

def SnapChain(bones, obArm, points, factor, radius = [], radiusFac = -1, roll = [], rollFac = -1):
    SnapChainToPoints(bones, obArm, points, factor, radius = radius, radiusFac = radiusFac, roll = roll, rollFac = rollFac)
    if (obArm.data.use_mirror_x):
        mirrorBones = []
        mirrorPoints = []
        for b in bones:
            mirrorBones.append( f_hier.FindSymmetryBone (b, obArm)[0])
        for pnt in points:
            dupePnt = pnt.copy()
            mPnt = Vector( (dupePnt[0] * -1, dupePnt[1], dupePnt[2]) )
            mirrorPoints.append(mPnt)

        SnapChainToPoints(mirrorBones, obArm, mirrorPoints, factor, radius = radius, radiusFac = radiusFac, roll = roll, rollFac = rollFac)
        #TODO  TODO maybe a hack?
        # check if the radius, etc work in symmetry

def FindBranchPoints(bones, obArm, checkBetweenJoints = True, tolerance = 0.05):
    if (obArm.mode == 'EDIT'):
        parents = []
        children = []
        for bone_a in bones:
                if ((bone_a in children) or (bone_a.parent != None)):
                    continue
                for bone_b in bones:
                    if (bone_a == bone_b):
                        continue
                    if (f_gen.CoincidentPoints(bone_a.head, bone_b.tail, tolerance)):
                        children.append(bone_a)
                        parents.append(bone_b)
                        continue
                    if (checkBetweenJoints):
                        if (bone_a.head == bone_b.head): #Probably not necesary, but harmless to check
                            continue
                        # Sanity check- if bone_a.head is farther away than the length of bone_b, skip
                        # Not taking the square root, hypothetically it's faster, but quick tests show it uses the same time
                        disp = (bone_a.head - bone_b.tail)
                        if( disp.dot(disp) > (bone_b.length + tolerance)**2 ):
                            continue
                        intersect = mathutils.geometry.intersect_point_line(bone_a.head, bone_b.head, bone_b.tail)
                        if ((0 <= intersect[1] <= 1) and (f_gen.CoincidentPoints(bone_a.head, intersect[0], tolerance))):
                            children.append(bone_a)
                            parents.append(bone_b)
                            bone_a.use_connect = False
    else: # Test this before using. I don't think I actually need this...
        parents = []
        children = []
        for bone_a in bones:
                if ((bone_a in children) or (bone_a.parent != None)):
                    continue
                for bone_b in bones:
                    if (bone_a == bone_b):
                        continue
                    if (f_gen.CoincidentPoints(bone_a.head_local, bone_b.tail_local, tolerance)):
                        children.append(bone_a)
                        parents.append(bone_b)
                        continue
                    if (checkBetweenJoints):
                        if (bone_a.head_local == bone_b.head_local): #Probably not necesary, but harmless to check
                            continue
                        # Sanity check- if bone_a.head_local is farther away than the length of bone_b, skip
                        # Not taking the square root, hypothetically it's faster, but quick tests show it uses the same time
                        disp = (bone_a.head_local - bone_b.tail_local)
                        if( disp.dot(disp) > (bone_b.length + tolerance)**2 ):
                            continue
                        intersect = mathutils.geometry.intersect_point_line(bone_a.head_local, bone_b.head_local, bone_b.tail_local)
                        if ((0 <= intersect[1] <= 1) and (f_gen.CoincidentPoints(bone_a.head_local, intersect[0], tolerance))):
                            children.append(bone_a)
                            parents.append(bone_b)
                            bone_a.use_connect = False
    return [(children[i], parents[i]) for i in range(len(children))] #a list of tuples containing child, parent

################################################################################
#Miscelaneaous Bone utilites
################################################################################

def SumLengthBones(bones):
    length = 0
    for b in bones:
        length += b.length
    return length

# def MidPointOfBones(bones):
#     #isn't really any good compared to bbCenter
#     #first, get a list of unique points from the bones:
#     heads = [b.head.copy() for b in bones]
#     #note, head is not valid for pose bone, use b.location instead
#     tails = [b.tail.copy() for b in bones]
#     points = heads + tails
#     #set requires frozen vectors
#     for h, t in zip(heads, tails):
#         h.freeze()
#         t.freeze()
#     points = list( set( points) )#gets unique points only
#     sumPoints = Vector( (0,0,0) )
#     for p in points:
#         sumPoints += p
#     return sumPoints/ len(bones)

#TODO: move bounding box from bones into own function
# and just use it for these two functions.
def BoundingBoxCenterBones(bones):
    #This is a better way to get the center point in 3D space
    xMin = float( 'inf')
    xMax = float('-inf')
    yMin = float( 'inf')
    yMax = float('-inf')
    zMin = float( 'inf')
    zMax = float('-inf')
    for b in bones:
        if (b.head.x < xMin):
            xMin = b.head.x
        if (b.head.x > xMax):
            xMax = b.head.x
        if (b.head.y < yMin):
            yMin = b.head.y
        if (b.head.y > yMax):
            yMax = b.head.y
        if (b.head.z < zMin):
            zMin = b.head.z
        if (b.head.z > zMax):
            zMax = b.head.z

        if (b.tail.x < xMin):
            xMin = b.tail.x
        if (b.tail.x > xMax):
            xMax = b.tail.x
        if (b.tail.y < yMin):
            yMin = b.tail.y
        if (b.tail.y > yMax):
            yMax = b.tail.y
        if (b.tail.z < zMin):
            zMin = b.tail.z
        if (b.tail.z > zMax):
            zMax = b.tail.z
    return Vector(((xMin + xMax)/2,(yMin + yMax)/2,(zMin + zMax)/2))

#TODO remove code duplication
def BoundingBoxVolumeBones(bones):
    xMin = float( 'inf')
    xMax = float('-inf')
    yMin = float( 'inf')
    yMax = float('-inf')
    zMin = float( 'inf')
    zMax = float('-inf')
    for b in bones:
        if (b.head.x < xMin):
            xMin = b.head.x
        if (b.head.x > xMax):
            xMax = b.head.x
        if (b.head.y < yMin):
            yMin = b.head.y
        if (b.head.y > yMax):
            yMax = b.head.y
        if (b.head.z < zMin):
            zMin = b.head.z
        if (b.head.z > zMax):
            zMax = b.head.z

        if (b.tail.x < xMin):
            xMin = b.tail.x
        if (b.tail.x > xMax):
            xMax = b.tail.x
        if (b.tail.y < yMin):
            yMin = b.tail.y
        if (b.tail.y > yMax):
            yMax = b.tail.y
        if (b.tail.z < zMin):
            zMin = b.tail.z
        if (b.tail.z > zMax):
            zMax = b.tail.z
    xLength = xMax - xMin
    yLength = yMax - yMin
    zLength = zMax - zMin
    return xLength*yLength*zLength

#######################################################################
# Operator Functions
#######################################################################

def CurveFromBones(bones,
                   obArm,
                   select_method,
                   combine_branches,
                   name,
                   spline_type,
                   is_evenly_distributed,
                   ):
    chains = ChainsInSelection( bones, 
                                obArm,
                                select_method = select_method,
                                combine_branches = combine_branches
                              )
    crv = []
    for chain in chains:
        crv.append( CurveFromChain(
            chain,
            obArm,
            name,
            spline_type,
            is_evenly_distributed
            ) )
    dictCrv = {}
    dictChain = {}
    i = 0
    for obCrv, chain in zip(crv, chains):
        if (obCrv):
            dictCrv[i]   = obCrv
            dictChain[i] = chain
            i +=1
            obCrv.data.dimensions = '3D'
    return dictCrv, dictChain 
    #TODO, return a list of tuples (i, crv, chain) instead

def CurveFromChain(chain, obArm, name, spline_type, is_evenly_distributed):
    ### CAN AND SHOULD REWRITE THIS FOR NEW IMPROVED POINTSFROMCURVE()
    # or is it OK how it is?
    obCrv = None
    if (chain is not None): #not necesary, but not harmful
        if (len(chain)>2):
            dataCrv = bpy.data.curves.new(name, 'CURVE')
            dataCrv.dimensions = '3D'
            # I should do something here to keep the numbers consistent
            obCrv = bpy.data.objects.new(name, dataCrv)
            points = PointsFromBoneChain(chain, obArm)
            circle = False
            if (points[0] == points[-1]):
                #if ((spline_type == 'BEZIER') or (spline_type == 'NURBS')):
                #why in the hell was this check here??
                # print ("Detected a circular chain")
                circle = True
                del points[-1] # is this wise?
                if ((spline_type == 'BEZIER') or (spline_type == 'NURBS')):
                    points = f_gen.rotate(points, -2)
                #Note, it's rotated twice because circular bone chains 
                #   convert to poly a little differently:
                #       They start at the second point instead of the first
                #       TODO need to test this a bit more for all cases
                #       also I know this isn't necesary for POLY type
            spl = f_curve.SplineFromPoints(obCrv, spline_type,
                                    points)
            if (spl):
                spl.use_cyclic_u = circle
                if (spline_type == 'NURBS'):
                    spl.use_bezier_u   = False
                    spl.use_endpoint_u = True
                if  (is_evenly_distributed):
                    factorsList = []
                    #
                    factors = [ j * (1/(len(chain) + 1)) for j in range(len(chain) + 1)]
                    factors.append(1)
                    factorsList.append(factors) #ugly
                    #
                    points = f_curve.PointsFromCurve(obCrv, factorsList)[0]
                    dataCrv.splines.remove(spl)
                    spl = f_curve.SplineFromPoints(obCrv, spline_type,
                                        points)
                    spl.use_cyclic_u = circle
                    if (spline_type == 'NURBS'):
                        spl.use_bezier_u   = False
                        spl.use_endpoint_u = True
    return obCrv

def RenameBoneChain(
      bone,
      obArm,
      name,
      symmetry,
      renameOpp,
      increment,
      offset,
      parent_extend,
      child_extend,
      select_method):
    chain = SelectChain(bone, obArm, select_method, parent_extend, child_extend)
    if (increment < 0):
        offset += ((len(chain) - 1) * increment * -1)
    opps = []
    chis = []
    oppChis = []
    for b in chain:
        chi = ''
        opp = None
        oppChi = ''
        opp, oppChi = f_hier.FindSymmetryBone(b, obArm)
        if (obArm.mode == 'EDIT'):
            headLoc = b.head
            tailLoc = b.tail
        else:
            headLoc = b.head_local
            tailLoc = b.tail_local
        
        if   (symmetry == 'X'):
            headMirror = Vector((headLoc[0] * -1, headLoc[1], headLoc[2]))
            tailMirror = Vector((tailLoc[0] * -1, tailLoc[1], tailLoc[2]))
        elif (symmetry == 'Y'):
            headMirror = Vector((headLoc[0], headLoc[1] * -1, headLoc[2]))
            tailMirror = Vector((tailLoc[0], tailLoc[1] * -1, tailLoc[2]))
        elif (symmetry == 'Z'):
            headMirror = Vector((headLoc[0], headLoc[1], headLoc[2] * -1))
            tailMirror = Vector((tailLoc[0], tailLoc[1], tailLoc[2] * -1))
        else:
            headMirror, tailMirror = None, None
        
        
        if (opp):
            if (obArm.mode == 'EDIT'):
                if (not f_gen.CoincidentPoints(opp.head, headMirror)):
                    opp = None
                elif (not f_gen.CoincidentPoints(opp.tail, tailMirror)):
                    opp = None
            else:
                if (not f_gen.CoincidentPoints(opp.head_local, headMirror)):
                    opp = None
                elif (not f_gen.CoincidentPoints(opp.tail_local, tailMirror)):
                    opp = None
        
        if (symmetry != 'NONE'):
            chi, foundChi, noChi = f_name.DetectChiral(b.name)
            #TODO:
            #      give the option here to enforce consistent names
            #        not a priority yet, waiting on implementation of
            #        ChainTools user preferences.
            if (foundChi):
                if (f_name.ChiAxis(chi) != symmetry):
                    foundChi = False
            if (not foundChi):
                if (symmetry == 'X'):
                    if (headLoc[0] > 0):
                        chi = ".L"
                    elif (headLoc[0] < 0):
                        chi = ".R" #TODO use preferences to set default chiral identifiers
                    else:
                        if   (tailLoc[0] > 0 ):
                            chi = ".L"
                        elif (tailLoc[0] < 0 ):
                            chi = ".R"
                        else:
                            chi = ''
                            # "."  #default Blender adds a dot and no suffix
                            #       in auto-symmetry naming operator 
                            # add option to add .C for center?
                            # I do this, but I don't think others do.
                elif (symmetry == 'Y'):
                    if (headLoc[1] > 0):
                        chi = ".Bk"
                    elif (headLoc[1] < 0):
                        chi = ".Fr"
                    else:
                        if   (tailLoc[1] > 0 ):
                            chi = ".Bk"
                        elif (tailLoc[1] < 0 ):
                            chi = ".Fr"
                        else:
                            chi = ''
                elif (symmetry == 'Z'):
                    if (headLoc[2] > 0):
                        chi = ".Top"
                    elif (headLoc[2] < 0):
                        chi = ".Bot"
                    else:
                        if   (tailLoc[2] > 0 ):
                            chi = ".Top"
                        elif (tailLoc[2] < 0 ):
                            chi = ".Bot"
                        else:
                            chi = ''

        chis.append(chi)
        opps.append(opp)
        oppChis.append(oppChi)
        b.name = f_name.randomString(32) #temporarily scramble name
        if ((opp) and (renameOpp == True)):
            opp.name = f_name.randomString(32)
        
    for j, b in enumerate(chain):  
        incr = ( j * increment ) + (offset)
        chi = chis[j]
        opp = opps[j]
        oppChi = oppChis[j]
        if (incr != 0):
            nameStub = name + '.' + str(incr).zfill(3)
        else:
            numbers = f_name.DetectNumber(b.name)[0]
            nameStub = name
            for num in numbers:
                nameStub = nameStub + '.' + str(num).zfill(3)
        b.name = nameStub + chi
        if (b.name != (nameStub + chi) ):
            print ("Bone %s could not be renamed to %s. Name already exists in armature." % (b.name, (nameStub + chi)))
        b.select      = True
        b.select_head = True
        b.select_tail = True
        if ((opp) and (renameOpp)):
            opp.name = nameStub + oppChi
            if (opp.name != (nameStub + oppChi) ):
                print ("Bone %s could not be renamed to %s. Name already exists in armature." % (b.name, (nameStub + oppChi)))
            opp.select      = True
            opp.select_head = True
            opp.select_tail = True

def GetChainAndBranchHeads(bone,
                       obArm,
                       select_method = {'NAME', 'LOCATION'},
                       parent_extend = False):
        useName = ('NAME' in select_method)
        useLocation = ('LOCATION' in select_method)
        useConstraint = ('CONSTRAINT' in select_method)
        useDisconnected = ('DISCONNECTED' in select_method)
        useConnected = ('CONNECTED' in select_method)
        return f_hier.GetBoneChain( bone,
                                    obArm,
                                    parent_extend=parent_extend,
                                    constraint=useConstraint,
                                    location=useLocation,
                                    name=useName,
                                    disconnected=useDisconnected,
                                    connected=useConnected, )
    
def SelectChain(bone,
                obArm,
                select_method = 'NAMELOC',
                parent_extend = False,
                child_extend  = False):
    chain, branches = GetChainAndBranchHeads( bone,
                                              obArm, 
                                              select_method,
                                              parent_extend)
    if (child_extend):
        while ((len(branches) > 0)):
            # print ("beginning While Loop")
            # print (branches)
            branchChain, branches = GetChainAndBranchHeads( branches[0],
                                                            obArm, 
                                                            select_method,
                                                            parent_extend)
            chain += branchChain
    return chain

def ChainsInSelection( bones,
                       obArm,
                       select_method = {'NAME', 'LOCATION'},
                       combine_branches = False):
        useName = ('NAME' in select_method)
        useLocation = ('LOCATION' in select_method)
        useConstraint = ('CONSTRAINT' in select_method)
        useDisconnected = ('DISCONNECTED' in select_method)
        useConnected = ('CONNECTED' in select_method)
        return f_hier.ChainFromBones(
            bones,
            obArm,
            combine_branches= combine_branches,
            constraint = useConstraint,
            location = useLocation,
            name = useName,
            disconnected = useDisconnected,
            connected = useConnected,
            )

def SetBoneDataFromRibbon(
                     bones,
                     chain,
                     obArm,
                     obCrv,
                     use_whole_chain = True,
                     splineIndex = 0,
                     fReport = None, ):
    #NOTE partial duplicate of f_editbone.SnapChainToRibbon
    #This is intentional; I want to keep the modes separate.
    #NOTE currently all this does is set the envelope radius
    dupeCrv = obCrv.copy()
    dupeCrv.data = obCrv.data.copy()
    crvRadius = 0
    if (dupeCrv.data.bevel_depth == 0):
        crvRadius = dupeCrv.data.extrude
    else:
        crvRadius = dupeCrv.data.bevel_depth
        dupeCrv.data.bevel_depth = 0
        dupeCrv.data.extrude = crvRadius
    if (crvRadius == 0):
        dupeCrv.data.extrude = bones[0].head_radius
    #now we definitely have a ribbon
    bonesInd = {}
    for i, b in enumerate(chain):
        if b in bones:
            bonesInd[b.name] = i
            if (use_whole_chain == True):
                uValues = uValuesInChain(chain, obArm)
            else:
                uValues = uValuesInChain(bones, obArm)
    factorsList = []
    for i in range(len(dupeCrv.data.splines)):
        factorsList.append(uValues)
    data = f_curve.DataFromRibbon(dupeCrv, factorsList)[splineIndex]
    width = data[1]
    # this function takes a list of factors and returns a list of points lists
    bPrev = None
    for i, b in enumerate(bones):
        b.envelope_distance = 0 #for testing
        b.head_radius = width[i]
        try:
            bPrev.tail_radius = width[i]
        except AttributeError:
            pass
        bPrev = b
    b.tail_radius = width[-1]
    bpy.data.curves.remove(dupeCrv.data) #removes the object, too