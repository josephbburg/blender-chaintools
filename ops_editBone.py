#Copyright (C) 2019 Joseph Brandenburg

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""Module for Edit-Mode operators"""
import bpy
import mathutils
from . import ops_base
from . import f_editBone
from . import f_hier
from . import f_bone
from . import f_gen
from . import f_name

class RelaxBoneChain(ops_base.ChainToolsOperator):
    bl_idname = "armature.relax_bone_chain"
    bl_label  = "Relax Bone Chain"
    bl_description = "Spaces bones out evenly along the selected chain"
    bl_options = {'REGISTER', 'UNDO'}
    select_method: bpy.props.EnumProperty(
                            items = ops_base.ChainToolsOperator.select_method_enum_flag,
                            name = "Selection Method",
                            options = {'ENUM_FLAG'},
                            default = f_gen.GetAddonPref("defaultSelectionMethod"),)
    factor: bpy.props.FloatProperty(
                    name = "Factor",
                    description = "How much to smooth the bone chain",
                    min = 0,
                    max = 1,
                    default = 0.5,
                    precision = 2,
                    subtype = 'FACTOR'
    )

    @classmethod
    def poll(cls, context):
        return ((context.active_object.type == 'ARMATURE') and (
                 context.mode == 'EDIT_ARMATURE'))
    def execute(self, context):
        if (self.factor == 0):
           return {'FINISHED'}
        if (context.mode == 'EDIT_ARMATURE'):
            bones = context.selected_editable_bones
        obArm= context.active_object
        f_editBone.RelaxBoneChains( bones,
                                  obArm,
                                  self.select_method,
                                  self.factor)
        f_hier.UpdateChildren(bones)
        # if (obArm.data.use_mirror_x):
        #     f_bone.UpdateMirrorBones(bones, obArm)
        return {'FINISHED'}

class SmoothBoneChain(ops_base.ChainToolsOperator):
    bl_idname = "armature.smooth_bone_chain"
    bl_label  = "Smooth Bone Chain"
    bl_description = "Smooths the selected bone chain"
    bl_options = {'REGISTER', 'UNDO'}
    select_method: bpy.props.EnumProperty(
                            items = ops_base.ChainToolsOperator.select_method_enum_flag,
                            name = "Selection Method",
                            options = {'ENUM_FLAG'},
                            default = f_gen.GetAddonPref("defaultSelectionMethod"),)
    factor: bpy.props.FloatProperty(
                    name = "Factor",
                    description = "How much to smooth the bone chain",
                    min = 0,
                    max = 1,
                    default = 0.5,
                    precision = 2,
                    subtype = 'FACTOR'
    )
    iterations: bpy.props.IntProperty(
                    name = "Iterations",
                    description = "How many times to perform the smoothing operation",
                    min = 1,
                    max = 256,
                    default = 1,
                    soft_max = 32,
                    subtype = 'UNSIGNED'
    )
    relax: bpy.props.FloatProperty(
                    name = "Relax",
                    description = "How much to affect bone spacing when smoothing",
                    min = 0,
                    max = 1,
                    default = 0.25,
                    precision = 2,
                    subtype = 'FACTOR'
    )
    preserveVol: bpy.props.FloatProperty(
                    name = "Preserve Volume",
                    description = "How much to preserve volume",
                    min = 0,
                    max = 1,
                    default = 0,
                    precision = 2,
                    subtype = 'FACTOR'
    )
    @classmethod
    def poll(cls, context):
        return ((context.active_object.type == 'ARMATURE') and (
                 context.mode == 'EDIT_ARMATURE'))
    
    def execute(self, context):
        if (self.factor == 0):
            return {'FINISHED'}
        if (context.mode == 'EDIT_ARMATURE'):
            bones = context.selected_editable_bones
        obArm = context.active_object
        f_editBone.SmoothBoneChains( bones,
                                   obArm,
                                   self.select_method,
                                   self.iterations,
                                   self.relax,
                                   self.preserveVol,
                                   self.factor,
                                 )
        f_hier.UpdateChildren(bones)
        # if (obArm.data.use_mirror_x):
        #     f_bone.UpdateMirrorBones(bones, obArm)
        return {'FINISHED'}

class CreateBonesAlongCurve(ops_base.ChainToolsOperator):
    """Create bones along curve"""
    bl_idname = "armature.create_bones_along_curve"
    bl_label = "Create Bones Along Curve"
    bl_options = {'REGISTER', 'UNDO'}

    #TODO: additional operator for object mode, adds bones to selected armature
    #TODO: option to use spline radius as envelope radius
    #TODO: option to use spline tilt to set bone roll
    #TODO: option to add symmetrically
    #      but mirror modifier should do this. My addon should check for that and
    #      name appropriately

    @classmethod
    def poll(cls, context):
        return ((context.active_object.type == 'ARMATURE') and (
                 context.mode == 'EDIT_ARMATURE'))

    def populate_curves_list(self, context):
        #eventually add curve icon
        retList = []
        curves = []
        i = 0
        for ob in context.selected_objects:
            #this way, selected objects are higher on the list
            if (ob.type == 'CURVE'):
                if (len (ob.data.splines) > 0):
                    data_add = (ob.name, ob.name, "Curve to Create Bones Along", i)
                    retList.append(data_add)
                    curves.append(ob.name)
                    i+=1
        for ob in bpy.data.objects:
            if ((ob.type == 'CURVE') and (ob.name not in curves)):
                if (len (ob.data.splines) > 0):
                    data_add = (ob.name, ob.name, "Curve to Create Bones Along", i)
                    retList.append(data_add)
                    i+=1
        if (i == 0):
            retList = [(None, None, None, 0)]
        return(retList)

    quantity: bpy.props.IntProperty(
                name="Bones Per Spline",
                description="Number of bones to add along each spline; 0 creates bones on Spline control points",
                default = 0,
                min = 0, # 0 is fit to spline points
                soft_max = 64,
                subtype='UNSIGNED')
    name: bpy.props.StringProperty(
                name="Name",
                description="Name of the bone chain; each bone will use this name and a number (or multiple numbers, if the curve contains multiple splines).",
                default = "BoneChain")
                #Shouldn't this take the curve's name by default?
    is_connected: bpy.props.BoolProperty(
                name="Connected Parents",
                description="Whether or not to connect each bone to the previous bone, or with disconnected parents.",
                default = True) #TODO this can be an ENUM property that gives more options
    parentBranch: bpy.props.BoolProperty(
                name="Parent Branches",
                description="Whether or not to create parents at branch points",
                default = True)
    target: bpy.props.EnumProperty(
                items = populate_curves_list,
                name = "Target Curve" )
    def execute(self, context):
        obArm = context.active_object
        try:
            obCrv = bpy.data.objects[self.target]
        except:
            obCrv = None
        if (obCrv is not None):
            f_editBone.BonesFromCurve(
                obArm,
                obCrv,
                quantity=self.quantity,
                connected=self.is_connected,
                parentBranch=self.parentBranch,
                baseName=self.name,)
        return {"FINISHED"}

class SnapBonesToCurve(ops_base.ChainToolsOperator):
    """Snap Bones To Curve"""
    bl_idname = "armature.snap_bones_to_curve"
    bl_label = "Snap Bones to Curve"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return ((context.active_object.type == 'ARMATURE') and (
                 context.mode == 'EDIT_ARMATURE'))

    def populate_curves_list(self, context):
        #eventually add curve icon
        retList = []
        curves = []
        i = 0
        for ob in context.selected_objects:
            #this way, selected objects are higher on the list
            if (ob.type == 'CURVE'):
                if (len (ob.data.splines) > 0):
                    data_add = (ob.name, ob.name, "Curve to Create Bones Along", i)
                    retList.append(data_add)
                    curves.append(ob.name)
                    i+=1
        for ob in bpy.data.objects:
            if ((ob.type == 'CURVE') and (ob.name not in curves)):
                if (len (ob.data.splines) > 0):
                    data_add = (ob.name, ob.name, "Curve to Create Bones Along", i)
                    retList.append(data_add)
                    i+=1
        if (i == 0):
            retList = [(None, None, None, 0)]
        return(retList)
        #COPIED FROM ABOVE, DE-DUPLICATE TODO TODO HACK HACK
        # I think I can do this by defining a subclass and inheriting.

    select_method: bpy.props.EnumProperty(
                            items = ops_base.ChainToolsOperator.select_method_enum_flag,
                            name = "Selection Method",
                            options = {'ENUM_FLAG'},
                            default = f_gen.GetAddonPref("defaultSelectionMethod"),)
    target: bpy.props.EnumProperty(
                items = populate_curves_list,
                name = "Target Curve" )
    splineIndex: bpy.props.IntProperty(
                name="Spline Index",
                description="Spline to be used for snapping bones to",
                default = 0,
                subtype='UNSIGNED')
    use_whole_chain: bpy.props.BoolProperty(
                name="Use Whole Chain",
                default = False,
                description = "Use whole chain when calculating u-value along curve used for snapping joints",
                ) #TODO add description, use better name
    pointsFac: bpy.props.FloatProperty(
                    name = "Snap Location",
                    description = "Amount to snap bone location to curve",
                    min = 0,
                    max = 1,
                    default = 1,
                    precision = 2,
                    subtype = 'FACTOR', )
    rollFac: bpy.props.FloatProperty(
                    name = "Snap Bone Roll",
                    description = "Amount to snap bone roll to curve tilt",
                    min = 0,
                    max = 1,
                    default = 1,
                    precision = 2,
                    subtype = 'FACTOR', )
    radFac: bpy.props.FloatProperty(
                    name = "Snap Envelope Radius",
                    description = "Amount to snap bone envelope radius to curve ",
                    min = 0,
                    max = 1,
                    default = 1,
                    precision = 2,
                    subtype = 'FACTOR', )

    def execute(self, context):
        obArm = context.active_object
        try:
            obCrv = bpy.data.objects[self.target]
        except:
            obCrv = None
        bones = context.selected_editable_bones
        bone = context.active_bone
        if (len(bones) < 2):
            self.report({'ERROR_INVALID_INPUT'}, "Select two or more bones")
            return {'CANCELLED'}
        if (self.splineIndex > (len(obCrv.data.splines) - 1) ):
            self.splineIndex = 0 #back to zero makes it wrap in the UI.
        elif (self.splineIndex < ((-1 * len(obCrv.data.splines) + 1)) ):
            self.splineIndex = 0
        if (obCrv is not None):
            chains = f_bone.ChainsInSelection(
                bones,
                obArm,
                self.select_method,
                False,
            )
            for bones in chains:
                if (bone in bones):
                    chain = f_bone.SelectChain(
                        bones[0],
                        obArm,
                        self.select_method,
                        False,
                        False,
                        )
                    f_editBone.SnapBonesToRibbonBySpline(
                        bones,
                        chain,
                        obArm,
                        obCrv,
                        self.use_whole_chain,
                        self.splineIndex,
                        snap_points = self.pointsFac,
                        snap_roll = self.rollFac,
                        snap_radius = self.radFac,
                        fReport = self.report, )
        # if ((success) and (obArm.data.use_mirror_x)):
        #     f_bone.UpdateMirrorBones(bones, obArm)
        return {"FINISHED"}

class InvertBoneChain(ops_base.ChainToolsOperator):
    """Invert Bone Chain"""
    bl_idname = "armature.invert_chain"
    bl_label = "Invert Chain"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return ((context.active_object.type == 'ARMATURE') and (
                 context.mode == 'EDIT_ARMATURE'))

    select_method: bpy.props.EnumProperty(
                            items = ops_base.ChainToolsOperator.select_method_enum_flag,
                            name = "Selection Method",
                            options = {'ENUM_FLAG'},
                            default = f_gen.GetAddonPref("defaultSelectionMethod"),)
    #ADD: Option to maintain Bone's u-value

    def execute(self, context):
        obArm = context.active_object
        bones = context.selected_editable_bones
        message = None
        chains = f_bone.ChainsInSelection(
            bones,
            obArm,
            select_method = self.select_method,
            combine_branches = True,
        )
        if (len(chains) < len(bones)):
            for chain in chains:
                #TODO - add a warning if the root bone is using connected parents
                if (chain[0].use_connect == True):
                    message = 'First bone in chain uses Connected Parenting, result may be unusual.'
                    rType = {"INFO"}
                f_editBone.InvertChain(chain, obArm)
        else:
            message = "No chains discovered using current selection method --try using a different selection method."
            rType = {"ERROR_INVALID_INPUT"}
        if (message):
            self.report(type=rType, message=message)
        return {"FINISHED"}

class CreateParentsAtChainBranch(ops_base.ChainToolsOperator):
    """Create Parents At Chain Branch"""
    bl_idname = "armature.create_parents_at_chain_branch"
    bl_label = "Create Parents At Chain Branch"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return ((context.active_object.type == 'ARMATURE') and (
                 context.mode == 'EDIT_ARMATURE'))
    checkBetweenJoints: bpy.props.BoolProperty(
                name="Check Between Joints",
                default = True,
                # description = "Use whole chain when calculating u-value along curve used for snapping joints",
                ) #TODO add description, use better name
    tolerance: bpy.props.FloatProperty(
                name="Tolerance",
                # description="Spline to be used for snapping bones to",
                default = 0.05,)

    def execute(self, context):
        obArm = context.active_object
        bones = context.selected_editable_bones
        f_hier.CreateParentsAtBranchPoints(bones, obArm, self.checkBetweenJoints, self.tolerance)
        return {"FINISHED"}

class CreateSocketBones(ops_base.ChainToolsOperator):
    """Create Sockets For Selected Bones"""
    bl_idname = "armature.create_socket_bones"
    bl_label = "Create Sockets For Selected Bones"
    bl_options = {'UNDO'} #Register causes problems if operator switches modes

    @classmethod
    def poll(cls, context):
        return ((context.active_object.type == 'ARMATURE') and (
                 context.mode == 'EDIT_ARMATURE'))
                 
    def FillPrefixList(self, context):
        enumList = [('____', "", 'No Prefix; leave the current prefix in place, if any.', 'NONE', 0)]
        prefixes = f_name.GetDictJSON(fileProp="JSONprefix")
        i=1
        for prefix, data in prefixes.items():
            #data contains [Type, Description, Icon]
            if (data[0] != "BONE"):
                continue
            #unique_identifier, name, description, icon, number 
            enumList.append( (prefix, prefix + " (" + data[1] + ")", data[1], data[2], i) )
            i+=1
        return enumList
    
    select_method: bpy.props.EnumProperty(
                            items = ops_base.ChainToolsOperator.select_method_enum_flag,
                            name = "Selection Method",
                            options = {'ENUM_FLAG'},
                            default = f_gen.GetAddonPref("defaultSelectionMethod"),)
    prefix:bpy.props.EnumProperty(
        items= FillPrefixList,
        name= "Prefix",
        description = "Use This Prefix",)
    name: bpy.props.StringProperty(
                name="Base Name",
                description="Base Name to use for socket bones - will use the current base name if left empty.",
                default = "")
    suffix: bpy.props.StringProperty(
                name="Name Suffix",
                description="Suffix to add to socket bone(s)",
                default = "Socket")
    retargetConstraints: bpy.props.BoolProperty(
                name="Retarget Constraints",
                description = "For every pose bone using the selected bones in a constraint, change the sub-target of the constraint to use the newly created socket bone. WARNING: This will modify unselected bones.",
                default = True,)
    copyConstraints: bpy.props.BoolProperty(
                name="Copy Constraints To Socket Bone",
                description = "Copy constraints from each bone to its newly created socket bone, and removes them.",
                default = False,)
    socketParent: bpy.props.EnumProperty(
                items =[('CURRENT_PARENT',"Use Current Parent", "Socket Bone be child of bone's previous parent", 0), 
                        ('PREVIOUS_SOCKET',"Use Previous Socket", "Socket Bone be child of previous socket bone (Creating a chain).", 1),
                        ('NEW_ROOT',"Create New Root", "Socket Bones will be children of a new root bone at the base of the chain.", 2),
                        ('NONE',"None", "Socket Bones will be created without parents.", 3),],
                name = "Socket Parent Method",
                default = 'PREVIOUS_SOCKET')
    constraintOptions: bpy.props.EnumProperty(
           name = "Connect to Socket:",
           description = "Use this method to connect the selected bone(s) to new socket bone(s).",
           items = [('PRNT',"Use Parent", "Make the selected bone(s) child of newly created socket bone(s)", 'BONE_DATA', 1),
                    ('ALL' ,"Copy Transform", "Use Copy Transform Constraint", 'CONSTRAINT_BONE', 2),
                    ('LOC' ,"Copy Location", "Use Copy Location Constraint", 'CONSTRAINT_BONE', 4),
                    ('ROT' ,"Copy Rotation", "Use Copy Rotation Constraint", 'CONSTRAINT_BONE', 8),
                    ('SCL' ,"Copy Scale", "Use Copy Scale Constraint",'CONSTRAINT_BONE', 16),],
           options = {'ENUM_FLAG'},
           default = {'PRNT'},)
    # ownerSpace: bpy.props.EnumProperty(
    #             items =[('WORLD',"World", "World Space", 0),
    #                     ('LOCAL',"Local", "Local Space", 1),
    #                     ('LOCAL_WITH_PARENT',"Local (With Parent)", "Local Space of parent", 2),
    #                     ('POSE',"Pose", "Pose Space", 3),],
    #             name = "Owner Space",
    #             description = "Owner Space of constraint, if enabled.",
    #             default = 'WORLD',)
    # targetSpace: bpy.props.EnumProperty(
    #             items =[('WORLD',"World", "World Space", 0),
    #                     ('LOCAL',"Local", "Local Space", 1),
    #                     ('LOCAL_WITH_PARENT',"Local (With Parent)", "Local Space of parent", 2),
    #                     ('POSE',"Pose", "Pose Space", 3),],
    #             name = "Target Space",
    #             description = "Target Space of constraint, if enabled.",
    #             default = 'WORLD',)
    # Will have a seperate operator for this

    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self, width=512)
        #this is cool!
        #Maybe I could use this for smooth/relax as a way to get around the 'REGISTER' bug

    def execute(self, context):
        obArm = context.active_object
        chains = f_bone.ChainsInSelection(context.selected_editable_bones, 
                                          obArm, self.select_method, combine_branches=False )
        socketBoneNames, boneGroups = [], []; rootBoneName = ''        
        for chain in chains:
            prevSocket, rootBone = None, None
            for bone in chain:
                socketName = f_name.NewNameFromBone(bone.name, bone.basename, self.prefix, self.name, self.suffix)
                socket = obArm.data.edit_bones.new(socketName)
                socket.head = bone.head.copy(); socket.tail = bone.tail.copy(); socket.roll = bone.roll
                socket.bbone_x = bone.bbone_x * 0.9; socket.bbone_z = bone.bbone_z * 0.9
                bone.use_connect = False; socket.use_connect = False
                if (self.socketParent == "CURRENT_PARENT"):
                    setattr(socket, "parent", bone.parent)
                    #otherwise, Python evaluates the parent bones themselves and makes them a single instance...
                elif (self.socketParent == "PREVIOUS_SOCKET"):
                    socket.parent = prevSocket
                    prevSocket = socket
                elif (self.socketParent == "NEW_ROOT"):
                    if (not rootBone):
                        rootBoneName = f_name.NewNameFromBone(socket.name, socket.basename, self.prefix, "", "Parent" )
                        rootBone = obArm.data.edit_bones.new(rootBoneName); rootBoneName = rootBone.name
                        rootBone.head = socket.head.copy(); rootBone.tail = socket.tail.copy(); rootBone.roll = socket.roll
                        rootBone.bbone_x = bone.bbone_x * 1.2; rootBone.bbone_z = bone.bbone_z * 1.2
                    socket.parent = rootBone
                socketBoneNames.append(socket.name)
                boneGroups.append( (bone.name, socket.name) )
                if ('PRNT' in self.constraintOptions):
                    bone.parent = socket
        # constraints
        addConstraints = False
        if (('ALL' in self.constraintOptions) or ('LOC' in self.constraintOptions) or
            ('ROT' in self.constraintOptions) or ('SCL' in self.constraintOptions)):
            addConstraints = True
        if ((self.copyConstraints) or (self.retargetConstraints) or (addConstraints)):
            from . import f_poseBone
            bpy.ops.object.mode_set(mode='POSE')
            if (self.retargetConstraints): #Retarget constraints
                f_poseBone.RetargetConstraints(obArm, boneGroups)
            if (self.copyConstraints):
                f_poseBone.MoveConstraints(obArm, boneGroups)
            if (addConstraints):
                for (bName, sName) in boneGroups:
                    bPB = obArm.pose.bones[bName]
                    sPB = obArm.pose.bones[sName]
                    constraints = []
                    if ('ALL' in self.constraintOptions):
                        constraints.append(bPB.constraints.new( "COPY_TRANSFORMS"))
                    if ('LOC' in self.constraintOptions):
                        constraints.append(bPB.constraints.new( "COPY_LOCATION"))
                    if ('ROT' in self.constraintOptions):
                        constraints.append(bPB.constraints.new( "COPY_ROTATION"))
                    if ('SCL' in self.constraintOptions):
                        constraints.append(bPB.constraints.new( "COPY_SCALE"))
                    for c in constraints:
                        c.target = obArm
                        c.subtarget = sPB.name
            bpy.ops.object.mode_set(mode='EDIT')
        # select newly created bones, deselect everything else
        for b in obArm.data.edit_bones:
            if ((b.name in socketBoneNames) or (b.name == rootBoneName)):
                b.select      = True
                b.select_head = True
                b.select_tail = True
            else: #could also check for visibility
                b.select      = False
                b.select_head = False
                b.select_tail = False
        return {"FINISHED"}

class ParentChainToChain(ops_base.ChainToolsOperator):
    """Parent Chain To Chain"""
    bl_idname = "armature.parent_chain_to_chain"
    bl_label = "Parent Chain to Chain"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return ((context.active_object.type == 'ARMATURE') and (
                 context.mode == 'EDIT_ARMATURE'))
    
    select_method: bpy.props.EnumProperty(
                            items = ops_base.ChainToolsOperator.select_method_enum_flag,
                            name = "Selection Method",
                            options = {'ENUM_FLAG'},
                            default = f_gen.GetAddonPref("defaultSelectionMethod"),)
    constraintOptions: bpy.props.EnumProperty(
           name = "Parent/Constrain Method:",
           description = "Use this method to connect the bone(s) in the active chain to the bone(s) in the selected chain.",
           items = [('PRNT',"Use Parent", "Make the selected bone(s) child of newly created socket bone(s)", 'BONE_DATA', 1),
                    ('ALL' ,"Copy Transform", "Use Copy Transform Constraint", 'CONSTRAINT_BONE', 2),
                    ('LOC' ,"Copy Location", "Use Copy Location Constraint", 'CONSTRAINT_BONE', 4),
                    ('ROT' ,"Copy Rotation", "Use Copy Rotation Constraint", 'CONSTRAINT_BONE', 8),
                    ('SCL' ,"Copy Scale", "Use Copy Scale Constraint",'CONSTRAINT_BONE', 16),],
           options = {'ENUM_FLAG', 'HIDDEN'},
           default = {'PRNT'},)

    def execute(self, context):
        obArm = context.active_object
        bAct = context.active_bone
        bones = context.selected_editable_bones
        chAct = None; chSel = None
        if (len(bones) < 2):
            self.report(type = {'ERROR_INVALID_INPUT'}, message="Select two or more bones.")
            return {'CANCELLED'}
        if (len(bones) == 2):
            chains = []
            for b in bones:
                chain = f_bone.SelectChain(
                    b,
                    obArm,
                    self.select_method,
                    False,
                    False,)
                chains.append(chain)
                if (b == bAct):
                    chAct = chain
                else:
                    if (chSel is None):
                        chSel = chain
                    # else:
                    #     self.report(type = {'ERROR_INVALID_INPUT'}, message="Active bone is not in either selected chain.")
                    #     return {'CANCELLED'}
        else:
            chains = f_bone.ChainsInSelection(bones, obArm, select_method = "self.select_method")
        
        if (len(chains) != 2):
            self.report(type = {'ERROR_INVALID_INPUT'}, message="Select two chains of equal length.")
            return {'CANCELLED'}
        if (len(chains[0]) != len(chains[1])):
            self.report(type = {'ERROR_INVALID_INPUT'}, message="Select two chains of equal length.")
            return {'CANCELLED'}

        if (chAct is None):
            chAct = chains[0] if (bAct in chains[0]) else None
            chAct = chains[1] if (bAct in chains[1]) else None
        if (chAct is None): #the above should have fixed it
            self.report(type = {'ERROR_INVALID_INPUT'}, message="Active bone is not in either selected chain.")
            return {'CANCELLED'}
        chSel = chains[0] if (chAct == chains[1]) else chains[1]
        # boneGroups = []
        for bAct, bSel in zip(chAct, chSel):
            if ('PRNT' in self.constraintOptions):
                bSel.use_connect = False
                bSel.parent = bAct
            # boneGroups.append( (bSel.name, bAct.name) )
        #Not being used right now
        # # constraints
        # addConstraints = False
        # if (('ALL' in self.constraintOptions) or ('LOC' in self.constraintOptions) or
        #     ('ROT' in self.constraintOptions) or ('SCL' in self.constraintOptions)):
        #     addConstraints = True
        # if (addConstraints):
        #     bpy.ops.object.mode_set(mode='POSE')
        #     for (bName, sName) in boneGroups:
        #         bPB = obArm.pose.bones[bName]
        #         sPB = obArm.pose.bones[sName]
        #         constraints = []
        #         if ('ALL' in self.constraintOptions):
        #             constraints.append(bPB.constraints.new( "COPY_TRANSFORMS"))
        #         if ('LOC' in self.constraintOptions):
        #             constraints.append(bPB.constraints.new( "COPY_LOCATION"))
        #         if ('ROT' in self.constraintOptions):
        #             constraints.append(bPB.constraints.new( "COPY_ROTATION"))
        #         if ('SCL' in self.constraintOptions):
        #             constraints.append(bPB.constraints.new( "COPY_SCALE"))
        #         for c in constraints:
        #             c.target = obArm
        #             c.subtarget = sPB.name
        #     bpy.ops.object.mode_set(mode='EDIT')
            
        return {"FINISHED"}